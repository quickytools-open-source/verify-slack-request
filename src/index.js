const crypto = require('crypto')

/**
 * Verifies a Slack request through an Express server
 *
 * @param {*} req Express request
 * @param {*} signingSecret Slack assigned app signing secret
 * @param {*} version Signing version number (defined by Slack)
 */
const isValidExpressServerSlackRequest = (req, signingSecret, version = 'v0') => {
  let isValid = false

  let bodyBuffer
  if (req.rawBody instanceof Buffer || req.body instanceof Buffer) {
    bodyBuffer = req.rawBody instanceof Buffer ? req.rawBody : req.body
  }

  const timestamp = req.header('x-slack-request-timestamp')
  const signature = req.header('x-slack-signature')
  if (version && timestamp && bodyBuffer && signature) {
    const body = Buffer.from(bodyBuffer).toString()
    isValid = isSignedSlackRequest(version, timestamp, body, signingSecret, signature)
  }
  return isValid
}

/**
 * Verifies a bare Slack request
 *
 * @param {*} version Signing version number (defined by Slack)
 * @param {*} timestamp `X-Slack-Request-Timestamp` request header
 * @param {*} body Original (string) request body
 * @param {*} signingSecret Slack assigned app signing secret
 * @param {*} signature `X-Slack-Signature` request header
 */
const isSignedSlackRequest = (version, timestamp, body, signingSecret, signature) => {
  const basestring = `${version}:${timestamp}:${body}`
  const hash = crypto.createHmac('sha256', signingSecret)
    .update(basestring)
    .digest('hex')
  return signature === `${version}=${hash}`
}

module.exports = {
  isValidExpressServerSlackRequest,
  isSignedSlackRequest
}
