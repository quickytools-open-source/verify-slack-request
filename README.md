# Verify Slack request signatures

Slack signs [requests](https://api.slack.com/docs/verifying-requests-from-slack). Verify bare or Express server Slack requests.

- `isSignedSlackRequest(version, timestamp, body, signingSecret, signature)`  
   Returns a `bool` by computing a signature from parts of the request and your app's `signingSecret` then comparing to the Slack request's expected `signature`.
- `isValidExpressServerSlackRequest(req, signingSecret, version = 'v0')`  
   Returns a `bool` by parsing a Slack request sent through an Express server.
