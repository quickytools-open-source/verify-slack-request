const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000

app.use(bodyParser.raw({ inflate: true, type: '*/*' }))

app.post('/', (req, res) => {
  const { isValidExpressServerSlackRequest } = require('../src/index')
  const isValid = isValidExpressServerSlackRequest(req, '8f742231b10e8888abcd99yyyzzz85a5')
  res.json({
    isValid,
    bodyBuffer: req.body instanceof Buffer,
    rawBodyBuffer: req.rawBody instanceof Buffer
  })
})

app.listen(port)

module.exports = app
