const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3001

var rawBodySaver = function (req, _res, buf) {
  if (buf && buf.length) {
    req.rawBody = buf
  }
}
app.use(bodyParser.urlencoded({ verify: rawBodySaver, extended: true }))

app.post('/', (req, res) => {
  const { isValidExpressServerSlackRequest } = require('../src/index')
  const isValid = isValidExpressServerSlackRequest(req, '8f742231b10e8888abcd99yyyzzz85a5')
  res.json({
    isValid,
    bodyBuffer: req.body instanceof Buffer,
    rawBodyBuffer: req.rawBody instanceof Buffer
  })
})

app.listen(port)

module.exports = app
