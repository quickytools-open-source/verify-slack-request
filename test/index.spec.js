const chai = require('chai')
chai.use(require('chai-http'))
const { expect } = chai

describe('index', () => {
  let subject

  beforeEach(() => {
    subject = require('../src/index')
  })

  it('isSignedSlackRequest', () => {
    const version = 'v0'
    const timestamp = '1531420618'
    const body = 'token=xyzz0WbapA4vBCDEFasx0q6G&team_id=T1DC2JH3J&team_domain=testteamnow&channel_id=G8PSS9T3V&channel_name=foobar&user_id=U2CERLKJA&user_name=roadrunner&command=%2Fwebhook-collect&text=&response_url=https%3A%2F%2Fhooks.slack.com%2Fcommands%2FT1DC2JH3J%2F397700885554%2F96rGlfmibIGlgcZRskXaIFfN&trigger_id=398738663015.47445629121.803a0bc887a14d10d2c447fce8b6703c'
    const signingSecret = '8f742231b10e8888abcd99yyyzzz85a5'
    const signature = 'v0=a2114d57b48eac39b9ad189dd8316235a7b4a8d21a10bd27519666489c69b503'
    expect(subject.isSignedSlackRequest(version, timestamp, body, signingSecret, signature))
  })

  describe('Express server', () => {
    it('isValidExpressServerSlackRequest(body)', (done) => {
      const app = require('./server')
      chai.request(app)
        .post('/')
        .set({ 'X-Slack-Request-Timestamp': '1531420618' })
        .set({ 'X-Slack-Signature': 'v0=a2114d57b48eac39b9ad189dd8316235a7b4a8d21a10bd27519666489c69b503' })
        .send('token=xyzz0WbapA4vBCDEFasx0q6G&team_id=T1DC2JH3J&team_domain=testteamnow&channel_id=G8PSS9T3V&channel_name=foobar&user_id=U2CERLKJA&user_name=roadrunner&command=%2Fwebhook-collect&text=&response_url=https%3A%2F%2Fhooks.slack.com%2Fcommands%2FT1DC2JH3J%2F397700885554%2F96rGlfmibIGlgcZRskXaIFfN&trigger_id=398738663015.47445629121.803a0bc887a14d10d2c447fce8b6703c')
        .end((_err, res) => {
          expect(res.status).to.eq(200)
          expect(res.body).to.eql({
            isValid: true,
            bodyBuffer: true,
            rawBodyBuffer: false
          })
          done()
        })
    })

    it('isValidExpressServerSlackRequest(rawBody)', (done) => {
      const app = require('./server-raw')
      chai.request(app)
        .post('/')
        .set({ 'X-Slack-Request-Timestamp': '1531420618' })
        .set({ 'X-Slack-Signature': 'v0=a2114d57b48eac39b9ad189dd8316235a7b4a8d21a10bd27519666489c69b503' })
        .send('token=xyzz0WbapA4vBCDEFasx0q6G&team_id=T1DC2JH3J&team_domain=testteamnow&channel_id=G8PSS9T3V&channel_name=foobar&user_id=U2CERLKJA&user_name=roadrunner&command=%2Fwebhook-collect&text=&response_url=https%3A%2F%2Fhooks.slack.com%2Fcommands%2FT1DC2JH3J%2F397700885554%2F96rGlfmibIGlgcZRskXaIFfN&trigger_id=398738663015.47445629121.803a0bc887a14d10d2c447fce8b6703c')
        .end((_err, res) => {
          expect(res.status).to.eq(200)
          expect(res.body).to.eql({
            isValid: true,
            bodyBuffer: false,
            rawBodyBuffer: true
          })
          done()
        })
    })
  })
})
