# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2021-04-13
### Changed
- Changed chai-http to be a dev dependency.

## [1.0.0] - 2019-12-26
### Added
- Verify requests for bare and Express requests.
